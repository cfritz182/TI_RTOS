/* EDUROBOTICA TECHCHALLENGE 2017
 *
 * BOM:
 * MSP-EXP432P401R BoosterPack
 * BLE Module
 *
 *
 * PIN-Configurations
 *
 * # 1
 * 3V3
 * P6.0 --> gpio!  DIO7_MRDY
 * P3.2 --> uarttx DIO0_TX
 * P3.3 --> uartrx DIO1_RX
 * P4.1 --> spiclk DIO10_CLK
 * P4.3 -->
 * P1.5 -->
 * P4.6 -->
 * P6.5 -->
 * P4.6 -->
 * P6.5 -->
 * P6.4 -->
 *
 *
 * # 21
 * 5V
 * GND
 * P6.1 -->
 * P4.0 -->
 * P4.2 -->
 * P4.4 -->
 * P4.5 -->
 * P4.7 -->
 * P5.4 --> gpio x-Value
 * P5.5 --> gpio y-Value
 *
 * # 40
 * P2.7 --> gpio DIRECTION_LEFT
 * P2.6 --> gpio DIRECTION_RIGHT
 * P2.4 --> PWM PWM_LEFT
 * P5.6 --> PWM PWM_RIGHT
 * P6.6 -->
 * P6.7 --> rst NRESET
 * P2.3 --> gpio ENCODER_RIGHT_A
 * P5.1 --> gpio ENCODER_RIGHT_B
 * P3.5 --> gpio ENCODER_LEFT_A
 * P3.7 --> gpio ENCODER_LEFT_B
 *
 * # 20
 * GND
 * P2.5 --> gpio! SRDY
 * P3.0 --> spics CS
 * P5.7 -->
 * RST -->
 * P1.6 --> spisimo UCB1MOSI
 * P1.7 --> spisomi UCB1MISO
 * P5.0 --> gpio! DIO13
 * P5.2 -->
 * P3.6 --> gpio! avoidance
 *
 *
 */
/* DriverLib Includes */


#include <stdint.h>

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <ti/sysbios/BIOS.h>

/* Driver header files */
#include <ti/drivers/GPIO.h>
/* Example/Board Header files */
#include "Board.h"

extern void *mainThread(void *arg0);
extern void *hallThread(void *arg0);
extern void *pwmThread(void *arg0);
extern void *adcThread(void *arg0);

/* Stack size in bytes */
#define THREADSTACKSIZE    1024

/***** GLOBAL VARIABLES *****/
pthread_mutex_t hold;
 uint_fast16_t Rx = 70;
 uint_fast16_t Ry = 70;
 int x=0;
 int y=0;

 extern void updatePosition();


/*
 *  ======== main ========
 */
int main(void)
{
    /************* MAIN THREAD************************/

    pthread_t           thread;
    pthread_attr_t      attrs,adcAttrs,pwmAttrs,hallAttrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    /* Call driver init functions */
    Board_initGeneral();

    /* Set priority and stack size attributes */
    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    detachState = PTHREAD_CREATE_DETACHED;
    retc = pthread_attr_setdetachstate(&attrs, detachState);
    if (retc != 0) {
        /* pthread_attr_setdetachstate() failed */
        while (1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    retc = pthread_create(&thread, &attrs, mainThread, NULL);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    if(pthread_mutex_init(&hold, NULL) != 0){
            /* pthread_mutex_init() failed */
            while (1);
        }

      /****ADC READING **************************/
      /* Set priority and stack size attributes */
    pthread_attr_init(&adcAttrs);
    priParam.sched_priority = 1;

    retc |= pthread_attr_setstacksize(&adcAttrs, THREADSTACKSIZE);
    if (retc != 0) {
          /* pthread_attr_setstacksize() failed */
          while (1);
      }

      pthread_attr_setschedparam(&adcAttrs, &priParam);

      retc = pthread_create(&thread, &adcAttrs, adcThread, NULL);
      if (retc != 0) {
          /* pthread_create() failed */
          while (1);
      }

      /************PWM**********************************/
        /* Set priority and stack size attributes */
        pthread_attr_init(&pwmAttrs);
        priParam.sched_priority = 1;

        retc |= pthread_attr_setstacksize(&pwmAttrs, THREADSTACKSIZE);
        if (retc != 0) {
            /* pthread_attr_setstacksize() failed */
            while (1);
        }

        pthread_attr_setschedparam(&pwmAttrs, &priParam);

        retc = pthread_create(&thread, &pwmAttrs, pwmThread, NULL);
        if (retc != 0) {
            /* pthread_create() failed */
            while (1);
        }
        /************HALL_THREAD**********************************/
              /* Set priority and stack size attributes */
              pthread_attr_init(&hallAttrs);
              priParam.sched_priority = 1;

              retc |= pthread_attr_setstacksize(&hallAttrs, THREADSTACKSIZE);
              if (retc != 0) {
                  /* pthread_attr_setstacksize() failed */
                  while (1);
              }

              pthread_attr_setschedparam(&hallAttrs, &priParam);

              retc = pthread_create(&thread, &hallAttrs, hallThread, NULL);
              if (retc != 0) {
                  /* pthread_create() failed */
                  while (1);
              }


    BIOS_start();

    return (0);
}
