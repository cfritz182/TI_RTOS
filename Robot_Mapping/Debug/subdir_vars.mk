################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../MSP_EXP432P401R_TIRTOS.cmd 

C_SRCS += \
../MSP_EXP432P401R.c \
../adc_joystick.c \
../gpiomain.c \
../hall.c \
../main_tirtos.c \
../pwm.c 

C_DEPS += \
./MSP_EXP432P401R.d \
./adc_joystick.d \
./gpiomain.d \
./hall.d \
./main_tirtos.d \
./pwm.d 

OBJS += \
./MSP_EXP432P401R.obj \
./adc_joystick.obj \
./gpiomain.obj \
./hall.obj \
./main_tirtos.obj \
./pwm.obj 

OBJS__QUOTED += \
"MSP_EXP432P401R.obj" \
"adc_joystick.obj" \
"gpiomain.obj" \
"hall.obj" \
"main_tirtos.obj" \
"pwm.obj" 

C_DEPS__QUOTED += \
"MSP_EXP432P401R.d" \
"adc_joystick.d" \
"gpiomain.d" \
"hall.d" \
"main_tirtos.d" \
"pwm.d" 

C_SRCS__QUOTED += \
"../MSP_EXP432P401R.c" \
"../adc_joystick.c" \
"../gpiomain.c" \
"../hall.c" \
"../main_tirtos.c" \
"../pwm.c" 


