/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
//#include <ti/display/Display.h>
//#include <ti/drivers/I2C.h>
// #include <ti/drivers/SDSPI.h>
// #include <ti/drivers/SPI.h>
// #include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header file */
#include "Board.h"

int hall_r_1_counter=0;
int hall_r_2_counter=0;
int hall_l_1_counter=0;
int hall_l_2_counter=0;

extern pthread_mutex_t hold;
//
//  ======== gpioButtonFxn0 ========
//  Callback function for the GPIO interrupt on Board_GPIO_BUTTON0.
//
void updatePosition(){
    //TODO

return;
}
void Fxn0(uint8_t index)
{
    hall_r_1_counter++;
}

void Fxn1(uint8_t index)
{
    hall_r_2_counter++;
}

void Fxn2(uint8_t index)
{
    hall_l_1_counter++;
}
void Fxn3(uint8_t index)
{
    hall_l_2_counter++;
}
void *hallThread(void *arg0)
{

    /* Call driver init functions */
    GPIO_init();
    // I2C_init();
    // SDSPI_init();
    // SPI_init();
    // UART_init();
    // Watchdog_init();

    /*configure & open Display driver */
   /* Display_Handle myDisplay;
    Display_Params params;
    Display_Params_init(&params);
    myDisplay = Display_open(Display_Type_UART, &params);
*/
    GPIO_setCallback(Board_GPIO_P2_3, Fxn0);
    GPIO_enableInt(Board_GPIO_P2_3);
    GPIO_setCallback(Board_GPIO_P5_1, Fxn1);
    GPIO_enableInt(Board_GPIO_P5_1);
    GPIO_setCallback(Board_GPIO_P3_5, Fxn2);
    GPIO_enableInt(Board_GPIO_P3_5);
    GPIO_setCallback(Board_GPIO_P3_7, Fxn3);
    GPIO_enableInt(Board_GPIO_P3_7);


    while (1) {
        usleep(2500);
       // Only update Serial terminal every 0.25s
        // TODO
        //updateposition();
        /*
        Display_printf(myDisplay, 0, 0, "HALL_R_A: %d / HALL_R_B: %d / HALL_L_A : %d /  HALL_L_B : %d / ", hall_r_1_counter, hall_r_2_counter, hall_l_1_counter,hall_l_2_counter);
*/
    }
    }
